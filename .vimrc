"--------------------------------------
"Super cool vimrc file -- Jeremy Luzzi
"---------------------------------------

"--------------
"UI Config
"--------------

syntax enable " enable syntax processing
set nu " show line numbers
set cursorline " highlight current line
set noshowmode "dissable default status bar

"-------------------
"Themeing
"-------------------
set background=dark " set the background color
packadd! onedark.vim
try
    color onedark "set the color theme
    let g:airline_theme='onedark'
catch
endtry

if(exists('+colorcolumn'))
    set colorcolumn=80
    highlight ColorColumn ctermbg=green
endif

"--------------------
"Spaces & Tabs
"--------------------

set tabstop=4 " number of visual spaces per tab
set softtabstop=4 " number of spaces tab when editing
set expandtab " tabs are spaces

"------------------------------
"File Navigation & Searching
"------------------------------

let g:unite_source_history_yank_enable = 1
try
    let g:unite_source_rec_async_command='ag --nocolor --nogroup -g ""'
    call unite#filters#matcher_default#use(['matcher_fuzzy'])
catch
endtry

" search a file in the filetree

" if vimproc can't be found by unite
nnoremap <space><space> :split<cr> :<C-u>Unite -start-insert file_rec<cr>
" if vimproc and unite are being chill bros
"nnoremap <space><space> :split<cr> :<C-u>Unite -start-insert file_rec/async<cr>

" reset
:nnoremap <space>r <Plug>(unite_restart)

" use <F2> to search the word under your cursor in the current directory
nmap <F2> :Ag <c-r>=expand("<cword>")<cr><cr>
noremap <space>/ :Ag
