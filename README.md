# JlzVimRc

---

** some super cool settings that I like to use for vim **

---

## Features :

**Search for a file**  
\<space\>\<space\>

**reset cache**  
\<space\>r

**Search for word under cursor**  
\<F2\>

**Open Ag**  
\<space\>/

## Installation :

install ag (the silver searcher) :  
    `brew install the_silver_searcher (MAC_OSX)`  
    or  
    `apt install the_silver_searcher (debian/ubuntu)`  

run install script :  
    `./install.sh`

## Resources :

Learn Vim progressivly :
http://yannesposito.com/Scratch/en/blog/Learn-Vim-Progressively/

Vim as an IDE :
http://yannesposito.com/Scratch/en/blog/Vim-as-IDE/

