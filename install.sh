#!/bin/bash
echo '--------------------'
echo '  STARTING INSTALL  '
echo '--------------------'
mkdir -p ~/.vim/pack/daddy/start
cd ~/.vim/pack/daddy/start
git clone https://github.com/rking/ag.vim
git clone https://github.com/bronson/vim-trailing-whitespace
git clone https://github.com/Shougo/unite.vim
git clone https://github.com/vim-airline/vim-airline
git clone https://github.com/Shougo/vimproc.vim
git clone https://github.com/vim-airline/vim-airline-themes
mkdir -p ~/.vim/pack/daddy/opt
cd ~/.vim/pack/daddy/opt
git clone https://github.com/joshdick/onedark.vim
cd ~/.vim/pack/daddy/vimproc.vim
./make
echo '--------------------'
echo '  FINISHED INSTALL  '
echo '--------------------'
